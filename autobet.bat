@echo off
mode con:cols=60 lines=5

cd %CD%

:start0
cls
set proc=%~n0.exe
set ti=5
tasklist | find "%proc%" >nul && (
echo %proc% is running
timeout /t %ti%
goto :start0) || (
start %proc%
timeout /t %ti%
goto :start0)
